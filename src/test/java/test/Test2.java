package test;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.*;

/**
 * Created by asv on 19.07.2017.
 */

public class Test2 {

    private static Logger LOGGER = LoggerFactory.getLogger(Test2.class);

    @Test
    public void testMethod1() {

        LOGGER.info("testMethod1 invoked");
        assertTrue(true);

    }

    @Test
    public void testMethod2() {

        LOGGER.info("testMethod2 invoked");
        assertTrue(true);
    }
}
