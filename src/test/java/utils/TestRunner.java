package utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by asv on 19.07.2017.
 */
public class TestRunner {

    private static Logger LOGGER = LoggerFactory.getLogger(TestRunner.class);

    /**
     *  counters of involved tests cases
     * */
    private int passedTests, failedTests;

    /**
     *  executes tests from formed JUnitTack
     *  if list of test methods is not specified in JUnitTack it will be generated from TestClass.getClass().getDeclaredMethods()
     * */
    public void start(JUnitTack jUnitTack) {

        jUnitTack.getTask().forEach((k ,v) -> {

            try {
                Object o = Class.forName(k).newInstance();

                OptionalConsumer.of(v)
                                .ifPresent(testCases -> runMethods(o, testCases))
                                .ifNotPresent(() -> runMethods(o, Arrays.stream(o.getClass().getDeclaredMethods())
                                                                        .map(Method::getName)
                                                                        .collect(Collectors.toList())));

            } catch (Exception e) {

                LOGGER.error("Error in Test class instance creation", e);
            }
        });

        printStatistic();
    }

    /**
     *  prints statistics after tests execution
     * */
    private void printStatistic() {

        LOGGER.info("Total test(s) - {}. Passed test(s) - {}. Failed test(s) - {}.",
                            (passedTests + failedTests), passedTests, failedTests);
    }

    /**
     *  invoke test method by reflection and handle exceptions during this process
     *  also fixes passed and failed tests methods
     * */
    private void runMethods( Object o, List<String> methods) {

        methods.forEach(method -> {

            try {

                o.getClass().getMethod(method).invoke(o);
                passedTests++;
            } catch (Exception e) {

                failedTests++;
                LOGGER.error("Exception in test method invocation.", e);
            }
        });
    }
}
