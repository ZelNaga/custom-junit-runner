package utils;

import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by asv on 19.07.2017.
 */

public class JunitTaskFromXml implements JUnitTack {

    private static Logger LOGGER = LoggerFactory.getLogger(JunitTaskFromXml.class);


    /**
    * custom xml data
    * */
    private static final String testSuiteXml = "<suite name=\"Suite1\">\n" +
                                               "<test name=\"Regression1\">\n" +
                                               "  <classes>\n" +
                                               "    <class name=\"test.Test1\">\n" +
                                               "      <methods>\n" +
                                               "        <include name=\"testMethod1\" />\n" +
                                               "      </methods>\n" +
                                               "    </class>\n" +
                                               "    <class name=\"test.Test2\" />\n" +
                                               "  </classes>\n" +
                                               " </test>\n" +
                                               "</suite>";

    /**
    * saves fixed problem during document parsing
    * */
    private boolean errorInInitialData;

    /**
     *  Map where key is test class name and value is optional which could keep list of test cases
     * */
    private Map<String, Optional<List<String>>> customTaskMap;

    public JunitTaskFromXml() {

        prepareData();
    }

    /**
     *  set object state and handle some exceptions during xml parsing
     * */
    private void prepareData() {

        try {

            customTaskMap = xmlToTaskMap();
        } catch (JDOMException | IOException e) {

            errorInInitialData = true;
            LOGGER.error("Exception in xml parsing !!!", e);
        }
    }

    /**
     *  transform xml to Map with test classes names and list of test cases which should be started
     * */
    private static Map<String, Optional<List<String>>> xmlToTaskMap() throws JDOMException, IOException {

        return new SAXBuilder().build(new ByteArrayInputStream(testSuiteXml.getBytes()))
                               .getRootElement()
                               .getChild("test")
                               .getChild("classes")
                               .getChildren()
                               .stream()
                               .collect(Collectors.toMap(k -> k.getAttributeValue("name"),
                                                         v -> Optional.ofNullable(v.getChild("methods"))
                                                                      .map(testCases -> testCases.getChildren("include")
                                                                                             .stream()
                                                                                             .map(testCase -> testCase.getAttributeValue("name"))
                                                                                             .collect(Collectors.toList()))));


    }


    /**
     *  returns fixed problem
     * */
    public boolean isValidInitialData() {
        return errorInInitialData;
    }

    /**
     *  returns formed task for TestRunner class
     * */
    @Override
    public Map<String, Optional<List<String>>> getTask() {
        return customTaskMap;
    }
}
