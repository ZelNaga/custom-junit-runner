package utils;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by asv on 19.07.2017.
 */
public interface JUnitTack {

    /**
     *  returns formed task for TestRunner class
     * */
    Map<String, Optional<List<String>>> getTask();
}
