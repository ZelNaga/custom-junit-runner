import com.google.common.base.Stopwatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.JunitTaskFromXml;
import utils.TestRunner;

/**
 * Created by asv on 19.07.2017.
 */
public class AppEntryPoint {

    private static Logger LOGGER = LoggerFactory.getLogger(AppEntryPoint.class);

    public static void main(String[] args) {

        Stopwatch sw = Stopwatch.createStarted();

        LOGGER.info("Start tests execution.");

        new TestRunner().start(new JunitTaskFromXml());

        LOGGER.info("End tests execution. Total elapsed time - {}", sw.stop());

    }
}
